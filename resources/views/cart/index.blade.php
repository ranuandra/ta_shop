@extends('layout.master')

@section('content')
    <div class="top-header"></div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header">Daftar Pesanan</h2>
        </div>
    </div>

<div class="row">
    <div class="col-sm-12">
        <article class="account-content">

            <form>
                    <div class="table-responsive">
                        <table class="table table-products">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Product</th>
                                <th>Unit Price</th>
                                <th>P x L</th>
                                <th>Jumlah</th>
                                <th>Subtotal</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contents as $content)
                            <tr>
                                <td class="col-xs-1">
                                    <img src="{{ URL('uploads/'.$content->product->file_image) }}" alt="" class="img-responsive img-responsive-2">
                                </td>
                                <td class="col-xs-3"><h4><a href="single-product.html">{{ $content->product->nama_product }}</a><small></small></h4></td>
                                <td class="col-xs-2"><span>{{ $content->product->harga }}</span></td>
                                <td class="col-xs-2">
                                        <div class="col-xs-6">
                                            <input type="text" name="panjang" class="form-control" value="{{ $content->options->panjang }}" >
                                        </div>
                                        <div class="col-xs-6">
                                            <input type="text" name="lebar" class="form-control" value="{{ $content->options->lebar }}" >
                                        </div>
                                </td>
                                <td class="col-xs-1">
                                        <div class="col-xs-12">
                                            <input type="text" name="quantity" class="form-control" value="{{ $content->qty }}" >
                                        </div>
                                </td>
                                <td>
                                    {{ $content->options->panjang*$content->options->lebar*$content->qty }}
                                    <input type="hidden" name="id" value="{{ $content->id }}" >
                                </td>
                                <td><a href="#" class="btn btn-danger"><i class="fa fa-times"></i> Hapus</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <a href="{{ $back_url }}" class="btn btn-inverse">Lanjut Belanja</a>
                    <a href="#" class="btn btn-inverse update-cart">Update Keranjang</a>
            </form>
                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Enter your coupon code if you have one.</h5>
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="Discount code">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">Apply Coupon</button>
                                    </span>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-2">
                            <ul class="list-unstyled order-total">
                                <li>Total products<span>$315.00</span></li>
                                <li>Discount<span>- $25.00</span></li>
                                <li>Subtotal<span class="total">$290.00</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <a href="{{ URL('checkout') }}" class="btn btn-primary btn-lg pull-right ">Checkout</a>
                </div>
            </form>

        </article>
    </div>
</div>

@endsection