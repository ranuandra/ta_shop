@extends('layout.master')

@section('content')
<div class="top-header"></div>

 <div class="row">
        <div class="col-sm-12">
            <article class="account-content checkout-steps">

                <div class="row row-no-padding">
                    <div class="col-xs-6 col-sm-3">
                        <div class="checkout-step active">
                            <div class="number">1</div>
                            <div class="title">Data Pemesan</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="checkout-step active">
                            <div class="number">2</div>
                            <div class="title">Pengiriman</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="checkout-step active">
                            <div class="number">3</div>
                            <div class="title">Pembayaran</div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="checkout-step">
                            <div class="number">4</div>
                            <div class="title">Review</div>
                        </div>
                    </div>
                </div>

                <div class="progress checkout-progress hidden-xs"><div class="progress-bar" role="progressbar" aria-valuenow="66.6" aria-valuemin="0" aria-valuemax="100" style="width:66.6%;"></div></div>

                {!! Form::open(array('url' => 'checkout-pembayaran', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                    <h3>Payment Method</h3>
                    <div class="products-order checkout payment-method">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 col-sm-10">
                                <div id="payment-methods" role="tablist" aria-multiselectable="true">
                                    @foreach($pembayarans as $pembayaran)
                                    <div class="panel radio">
                                        <input type="radio" name="pembayaran_id" value="{{ $pembayaran->id }}" id="radio-payment-{{ $pembayaran->id }}">
                                        <label for="radio-payment-{{ $pembayaran->id }}" data-toggle="collapse" data-target="#parent-{{ $pembayaran->id }}" data-parent="#payment-methods" aria-controls="parent-{{ $pembayaran->id }}">{{ $pembayaran->nama_pembayaran }}</span></label>
                                        <div id="parent-{{ $pembayaran->id }}" class="panel-collapse collapse in" role="tabpanel"></div>
                                    </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <input type="submit" class="btn btn-primary btn-lg pull-right" value="Lanjut">
                    </div>
                </form>
            </article>
        </div>
    </div>
@endsection
