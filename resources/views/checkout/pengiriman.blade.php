@extends('layout.master')

@section('content')
<div class="top-header"></div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header"></h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <article class="account-content checkout-steps">

            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">1</div>
                        <div class="title">Data Pemesan</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">2</div>
                        <div class="title">Pengiriman</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step">
                        <div class="number">3</div>
                        <div class="title">Pembayaran</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step">
                        <div class="number">4</div>
                        <div class="title">Review</div>
                    </div>
                </div>
            </div>

            <div class="progress checkout-progress hidden-xs"><div class="progress-bar" role="progressbar" aria-valuenow="33.3" aria-valuemin="0" aria-valuemax="100" style="width:33.3%;"></div></div>

            <div class="col-sm-12">
            {!! Form::open(array('url' => 'checkout-pengiriman', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                <h3>Pengiriman</h3>
                <div class="products-order checkout shipping-method">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="shipping-methods" role="tablist" aria-multiselectable="true">
                                     @foreach($pengirimans as $pengiriman)
                                        <div class="radio">

                                            <input type="radio"  name="pengiriman_id" value="{{ $pengiriman->id }}" id="radio-shipping-{{ $pengiriman->id }}">
                                            {{ $pengiriman->nama_pengiriman }}
                                        </div>
                                     @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <input type="submit" class="btn btn-primary btn-lg pull-right" value="Lanjut">
                </div>
            </form>
            </div>

        </article>
    </div>
</div>

@endsection