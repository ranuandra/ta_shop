@extends('layout.master')

@section('content')
<div class="top-header"></div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header">Data Pemesan</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <article class="account-content checkout-steps">

            <div class="row row-no-padding">
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">1</div>
                        <div class="title">Data Pemesan</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step">
                        <div class="number">2</div>
                        <div class="title">Pengiriman</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step">
                        <div class="number">3</div>
                        <div class="title">Pembayaran</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step">
                        <div class="number">4</div>
                        <div class="title">Review</div>
                    </div>
                </div>
            </div>

            <div class="progress checkout-progress hidden-xs"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0;"></div></div>

            {!! Form::open(array('url' => 'checkout', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                <h3>Data Pemesan</h3>
                <div class="products-order checkout billing-information">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Nama <span class="required">*</span></label>
                            @if($billing != null)
                                <input type="text" name="nama" value="{{ $billing->options->nama }}" class="form-control">
                            @else
                                <input type="text" name="nama" value="" class="form-control">
                            @endif

                        </div>
                        <div class="form-group col-sm-6">
                            <label>Email address <span class="required">*</span></label>
                            @if($billing != null)
                                <input type="text" name="email" value="{{ $billing->options->email }}" class="form-control">
                            @else
                                <input type="text" name="email" value="" class="form-control">
                            @endif
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Alamat <span class="required">*</span></label>
                            @if($billing != null)
                                <input type="text" name="alamat" value="{{ $billing->options->alamat }}" class="form-control">
                            @else
                                <input type="text" name="alamat" value="" class="form-control">
                            @endif
                        </div>
                   </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Negara</label>
                            @if($billing != null)
                                {!! Form::select('negara_id', $negaras, $billing->options->negara_id, ['class' => 'form-control']) !!}
                            @else
                                {!! Form::select('negara_id', $negaras, '', ['class' => 'form-control']) !!}
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Provinsi</label>
                            @if($billing != null)
                                {!! Form::select('provinsi_id', $provinsis, $billing->options->provinsi_id, ['class' => 'form-control']) !!}
                            @else
                                {!! Form::select('provinsi_id', $provinsis, '', ['class' => 'form-control']) !!}
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Kota</label>
                            @if($billing != null)
                                {!! Form::select('kota_id', $kotas, $billing->options->kota_id, ['class' => 'form-control']) !!}
                            @else
                                {!! Form::select('kota_id', $kotas, '', ['class' => 'form-control']) !!}
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>No Telp <span class="required">*</span></label>
                            @if($billing != null)
                                <input type="text" name="no_telp" value="{{ $billing->options->no_telp }}" class="form-control">
                            @else
                                <input type="text" name="no_telp" value="" class="form-control">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <input type="submit" class="btn btn-primary btn-lg pull-right" value="Lanjut">
                </div>
            </form>

        </article>
    </div>
</div>
@endsection