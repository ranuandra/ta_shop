@extends('layout.master')

@section('content')
<div class="top-header"></div>

<div class="row">
    <div class="col-sm-12">
        <article class="account-content checkout-steps">

            <div class="row row-no-padding">
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">1</div>
                        <div class="title">Data Pemesan</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">2</div>
                        <div class="title">Pengiriman</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="checkout-step active">
                        <div class="number">3</div>
                        <div class="title">Pembayaran</div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 active">
                    <div class="checkout-step">
                        <div class="number">4</div>
                        <div class="title">Review</div>
                    </div>
                </div>
            </div>

            <div class="progress checkout-progress hidden-xs"><div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%;"></div></div>

            {!! Form::open(array('url' => 'checkout-save', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                <h3>Review Order</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box">
                            <h4>Alamat</h4>
                            <ul class="list-unstyled">
                                <li><b>{{ $billing->options->nama }}</b></li>
                                <li>{{ $billing->options->alamat }}</li>
                                <li>{{ $kota->nama_kota }}</li>
                                <li>{{ $kota->GetProvinsi->nama_provinsi }}, {{ $kota->GetProvinsi->GetNegara->nama_negara }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box">
                            <h4>Pembayaran</h4>
                            <ul class="list-unstyled">
                                <li>{{ $pembayaran->pembayaran->nama_pembayaran }}</li>
                            </ul>
                        </div>
                        <div class="box">
                            <h4>Pengiriman</h4>
                            <ul class="list-unstyled">
                                <li>{{ $shipping->pengiriman->nama_pengiriman }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box">
                            <h4>Order Details</h4>
                            <ul class="list-unstyled">
                                <li><b>Email: </b>{{ $billing->options->email }}</li>
                                <li><b>Phone: </b>{{ $billing->options->no_telp }}</li>
                            </ul>
                            <h5>Addition information:</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat mauris eget magna egestas porta. Curabitur sagittis sagittis neque rutrum congue.</p>
                        </div>
                    </div>
                </div>

                <div class="products-order checkout shopping-cart">
                    <div class="table-responsive">
                        <table class="table table-products">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Product</th>
                                <th>Unit Price</th>
                                <th>Ukuran</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td class="col-xs-1"><img src="{{ URL('uploads/'.$order->product->file_image) }}" alt="" class="img-responsive"></td>
                                <td class="col-xs-4 col-md-5"><h4><a href="single-product.html">{{ $order->product->nama_product }}</a><small></small></h4></td>
                                <td class="col-xs-2 text-center"><span>{{ $order->product->harga }}</span></td>
                                <td class="col-xs-2 col-md-1 text-center"><span><b>{{ $order->options->panjang }} x {{ $order->options->lebar }}</b></span></td>
                                <td class="col-xs-2 text-center"><span><b>$60</b></span></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <ul class="list-unstyled order-total">
                        <li>Total products<span>$315.00</span></li>
                        <li>Discount<span>- $25.00</span></li>
                        <li>Shipping<span>$15.00</span></li>
                        <li>Subtotal<span class="total">$305.00</span></li>
                    </ul>
                </div>
                <div class="clearfix">
                    <div class="checkbox pull-left">
                        <input type="checkbox" id="checkout-terms-conditions">
                        <label for="checkout-terms-conditions">I have read and agree to the <a href="terms-conditions.html" target="_blank">Terms & Conditions</a></label>
                    </div>
                    <a href="{{ URL('checkout-save') }}" class="btn btn-primary btn-lg pull-right ">Confirm Order</a>
                </div>
            </form>
        </article>
    </div>
</div>
@endsection