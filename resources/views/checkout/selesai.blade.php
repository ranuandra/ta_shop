@extends('layout.master')

@section('content')
    <!-- ==========================
        BREADCRUMB - START
    =========================== -->
    <section class="breadcrumb-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h2>Checkout</h2>
                    <p>Shipping Method</p>
                </div>
                <div class="col-xs-6">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                        <li class="active">Shipping Method</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
        BREADCRUMB - END
    =========================== -->

    <!-- ==========================
        MY ACCOUNT - START
    =========================== -->
    <section class="content account">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <article class="account-content checkout-steps">

                    Terima kasih telah melakukan pemesanan product di tempat kami

                    </article>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
        MY ACCOUNT - END
    =========================== -->
@endsection