@extends('layout.master')

@section('content')
    <div class="top-header"></div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header">Daftar Cara Pengiriman</h2>
        </div>
    </div>

    <div class="row">
        @include('layout.menuadmin')
        <div class="col-sm-9">
            <article class="account-content">
                <h3>Pengiriman Baru</h3>
                {!! Form::open(array('url' => 'admin/pengiriman/save', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                    <input type="hidden" name="id" value="{{ $data->id }}" />

                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Nama Pengiriman<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            <input type="text" class="form-control" name="nama_pengiriman" value="{{ $data->nama_pengiriman }}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Aktif<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            @if($data->aktif == 1)
                                {!! Form::checkbox('aktif',1, true) !!}

                            @else
                                {!! Form::checkbox('aktif', false) !!}

                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8 col-lg-offset-3 col-lg-9">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </article>
        </div>
    </div>


@endsection

