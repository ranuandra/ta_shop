@extends('layout.master')

@section('content')
    <div class="top-header"></div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header">Daftar Product</h2>
        </div>
    </div>

    <div class="row">
        @include('layout.menuadmin')
        <div class="col-sm-9">
            <article class="account-content">
                <h3>Product</h3>
                <div class="table-responsive border">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nama Product</th>
                                <th>Kategori</th>
                                <th>Berat /m2</th>
                                <th>Harga /m2</th>
                                <th>Aktif</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{ $data->nama_product }}</td>
                            <td>{{ $data->GetKategori->nama_kategori }}</td>
                            <td>{{ $data->berat }}</td>
                            <td>{{ $data->harga }}</td>
                            <td>
                                @if($data->aktif == 1)
                                    <i class="fa fa-check"></i>
                                @else
                                    <i class="fa fa-times"></i>
                                @endif
                            </td>
                            <td><a href="{{ URL('admin/product/edit/'.$data->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pagination"> {{ $datas->links() }} </div>
                <div>
                    <a href="{{ URL('admin/product/create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah Product</a>
                </div>
            </article>
        </div>
    </div>


@endsection

