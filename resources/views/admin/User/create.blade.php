@extends('layout.master')

@section('content')
    <div class="top-header"></div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header">Daftar User</h2>
        </div>
    </div>


    <div class="row">
        @include('layout.menuadmin')
        <div class="col-sm-9">
            <article class="account-content">
                <h3>User Baru</h3>
                {!! Form::open(array('url' => 'admin/user/save', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                    <input type="hidden" name="id" value="{{ $data->id }}" />

                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Nama<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            <input type="text" class="form-control" name="name" value="{{ $data->name }}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Username<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            <input type="text" class="form-control" name="username" value="{{ $data->username }}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Email<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            <input type="text" class="form-control" name="email" value="{{ $data->email }}" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Password<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            <input type="password" class="form-control" name="password" value="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-lg-3 control-label">Role<span class="required">*</span></label>
                        <div class="col-md-6 col-lg-6">
                            {!! Form::select('roles[]', $roles, $uroles,['class' => 'form-control', 'multiple' => 'multiple']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8 col-lg-offset-3 col-lg-9">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </article>
        </div>
    </div>


@endsection

