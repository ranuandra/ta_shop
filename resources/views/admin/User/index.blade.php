@extends('layout.master')

@section('content')
    <div class="top-header"></div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-header">Daftar User</h2>
        </div>
    </div>

    <div class="row">
        @include('layout.menuadmin')
        <div class="col-sm-9">
            <article class="account-content">
                <h3>Pengelolaan User</h3>
                <div class="table-responsive border">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($datas as $data)
                        <tr>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->username }}</td>
                            <td>{{ $data->email }}</td>
                            <td>
                                @foreach($data->getRoles() as $role)
                                    {{ $role }}
                                @endforeach
                            </td>
                            <td><a href="{{ URL('admin/user/edit/'.$data->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pagination"> {{ $datas->links() }} </div>
                <div>
                    <a href="{{ URL('admin/user/create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah User</a>
                </div>
            </article>
        </div>
    </div>


@endsection

