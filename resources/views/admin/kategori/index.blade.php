@extends('layout.master')

@section('content')
<div class="top-header"></div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header">Daftar Kategori</h2>
    </div>
</div>

<div class="row">
    @include('layout.menuadmin')
    <div class="col-sm-9">
        <article class="account-content">
            <h3>Kategori Product</h3>
            <div class="table-responsive border">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nama Kategori</th>
                            <th>Aktif</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $data->nama_kategori }}</td>
                        <td>
                            @if($data->aktif == 1)
                                <i class="fa fa-check"></i>
                            @else
                                <i class="fa fa-times"></i>
                            @endif
                        <td><a href="{{ URL('admin/kategori-product/edit/'.$data->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination"> {{ $datas->links() }} </div>
            <div>
                <a href="{{ URL('admin/kategori-product/create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah Kategori</a>
            </div>
        </article>
    </div>
</div>

@endsection

