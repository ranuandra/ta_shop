@extends('layout.master')

@section('content')
    <!-- ==========================
    	BREADCRUMB - START
    =========================== -->
    <section class="breadcrumb-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <h2>My Account</h2>
                    <p>Account Dashboard</p>
                </div>
                <div class="col-xs-6">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="my-account.html">My Account</a></li>
                        <li class="active">Account Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
        BREADCRUMB - END
    =========================== -->

    <!-- ==========================
    	MY ACCOUNT - START
    =========================== -->
    <section class="content account">
        <div class="container">
            <div class="row">
                @include('layout.menuadmin')
                <div class="col-sm-9">
                    <article class="account-content">
                        <h3>Status Transaksi Baru</h3>
                        {!! Form::open(array('url' => 'admin/status-transaksi/save', 'class' => 'form-horizontal', 'role' => 'form')) !!}
                            <input type="hidden" name="id" value="{{ $data->id }}" />

                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label">Status Transaksi<span class="required">*</span></label>
                                <div class="col-md-6 col-lg-6">
                                    <input type="text" class="form-control" name="status_transaksi" value="{{ $data->status_transaksi }}" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 col-lg-3 control-label">Aktif<span class="required">*</span></label>
                                <div class="col-md-6 col-lg-6">
                                    @if($data->aktif == 1)
                                        {!! Form::checkbox('aktif',1, true) !!}

                                    @else
                                        {!! Form::checkbox('aktif', false) !!}

                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8 col-lg-offset-3 col-lg-9">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <!-- ==========================
    	MY ACCOUNT - END
    =========================== -->

@endsection

