@extends('layout.master')

@section('content')
<div class="top-header"></div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header">Daftar Transaksi</h2>
    </div>
</div>


<div class="row">
    @include('layout.menuadmin')
    <div class="col-sm-10">
        <article class="account-content">
            <h3>Daftar Transaksi</h3>
            <div class="table-responsive border">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Pemesan</th>
                            <th>Product</th>
                            <th>No Telp</th>
                            <th>Pembayaran</th>
                            <th>Pengiriman</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($datas as $data)
                    <tr>
                        <td>{{ $data->tanggal_transaksi }}</td>
                        <td>
                            {{ $data->GetPembeli->nama }}<br>
                            {{ $data->GetPembeli->alamat }}<br>
                            {{ $data->GetPembeli->GetKota->nama_kota }},<br>
                            {{ $data->GetPembeli->GetKota->GetProvinsi->nama_provinsi }}<br>
                            {{ $data->GetPembeli->GetKota->GetProvinsi->GetNegara->nama_negara }}
                        </td>
                        <td>
                            @foreach($data->GetDetails as $detail)
                                {{ $detail->GetProduct->nama_product }}<br>
                            @endforeach

                        </td>
                        <td>
                            {{ $data->GetPembeli->no_telp }}
                        </td>
                        <td>
                            {{ $data->GetPembayaran->nama_pembayaran }}
                        </td>
                        <td>
                            {{ $data->GetPengiriman->nama_pengiriman }}
                        </td>
                        <td>
                            {{ $data->GetStatusTransaksi->status_transaksi }}
                        </td>
                        <td><a href="{{ URL('admin/transaksi/edit/'.$data->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination"> {{ $datas->links() }} </div>
        </article>
    </div>
</div>


@endsection

