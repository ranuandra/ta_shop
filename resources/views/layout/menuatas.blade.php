<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Project name</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{ URL('/') }}">Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produk <span class="caret"></span></a>
                            @inject('menukategori', 'App\Helper\MenuKategoriHelper')
                            <ul class="dropdown-menu">
                                @foreach($menukategori->KategoriMenu() as $kategori)
                                    <li><a href="{{ URL('product/'.$kategori->id) }}">{{ $kategori->nama_kategori }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @role('administrator')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administrator <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL('admin/kategori-product') }}">Kategori</a></li>
                                <li><a href="{{ URL('admin/product') }}">Product</a></li>
                                <li><a href="{{ URL('admin/negara') }}">Negara</a></li>
                                <li><a href="{{ URL('admin/provinsi') }}">Provinsi</a></li>
                                <li><a href="{{ URL('admin/kota') }}">Kota</a></li>
                                <li><a href="{{ URL('admin/pengiriman') }}">Pengiriman</a></li>
                                <li><a href="{{ URL('admin/pembayaran') }}">Pembayaran</a></li>
                                <li><a href="{{ URL('admin/transaksi') }}">Transaksi</a></li>
                            </ul>
                        </li>
                        @endrole
                        @if(Auth::check())
                            <li><a href="{{ URL('logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ URL('login') }}">Login</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>








