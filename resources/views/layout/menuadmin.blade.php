<div class="col-sm-2">
    <ul class="nav nav-pills">
        <li class="active" role="presentation"><a href="{{ URL('admin/transaksi') }}">Transaksi</a></li>
        <li role="presentation"><a href="my-account.html">Master</a></li>
        <li role="presentation" ><a href="{{ URL('admin/kategori-product') }}">Kategori</a></li>
        <li role="presentation" ><a href="{{ URL('admin/product') }}">Product</a></li>
        <li role="presentation" ><a href="{{ URL('admin/negara') }}">Negara</a></li>
        <li role="presentation" ><a href="{{ URL('admin/provinsi') }}">Provinsi</a></li>
        <li role="presentation" ><a href="{{ URL('admin/kota') }}">Kota</a></li>
        <li role="presentation" ><a href="{{ URL('admin/pengiriman') }}">Pengiriman</a></li>
        <li role="presentation" ><a href="{{ URL('admin/pembayaran') }}">Pembayaran</a></li>
    </ul>

</div>