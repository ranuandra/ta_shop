@extends('layout.master')

@section('content')
    <div class="top-header-2"></div>

    <div class="row">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
        {!! Form::open(array('url' => 'login', 'class' => 'form-signin', 'role' => 'form')) !!}
            <h2 class="form-signin-heading">Silahkan Login</h2>
            <label for="inputEmail" class="sr-only">Username</label>
            <input type="text" id="inputEmail" class="form-control" name="username" placeholder="Username" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
        </div>
        <div class="col-xs-4"></div>
    </div>
@endsection

