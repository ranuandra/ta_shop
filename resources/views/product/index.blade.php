@extends('layout.master')

@section('content')
<div class="top-header"></div>
<div class="row">
    <div class="col-sm-12">
        <h2 class="page-header">Daftar Product</h2>
    </div>
 </div>

<div class="row">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked">
            @inject('menukategori', 'App\Helper\MenuKategoriHelper')

            @foreach($menukategori->KategoriMenu() as $kategori)
                <li role="presentation" ><a href="{{ URL('product/'.$kategori->id) }}}">{{ $kategori->nama_kategori }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="col-sm-9">
        <h2 class="page-header">{{ $data->nama_kategori }}</h2>
        @foreach($data->GetProducts as $product)
            <div class="row product-list">
                {!! Form::open(array('url' => 'cart', 'class' => 'form-horizontal', 'role' => 'form', 'files' => true)) !!}
                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                <div class="col-sm-3">
                    <img src="{{ URL::asset('uploads/'.$product->file_image) }}" class="img-responsive" alt="">
                </div>
                <div class="col-sm-9">
                    <h3>{{ $product->nama_product }}</h3>
                    <div class="row">
                        <div class="col-sm-2">Hargan: </div>
                        <div class="col-sm-3">{{ $product->harga }}</div>

                    </div>
                    <div class="row">
                        <div class="col-sm-2">Ukuran: </div>
                        <div class="col-sm-2"><input type="text" name="panjang" class="form-control" value=""> </div>
                        <div class="col-sm-2"><input type="text" name="lebar" class="form-control" value=""> </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Jumlah: </div>
                        <div class="col-sm-2"><input type="text" name="qty" class="form-control" value=""></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">File Corel: </div>
                        <div class="col-sm-3">{!! Form::file('file_cdr') !!}</div>
                    </div>
                    <input type="submit" value="Pesan" />
                </div>

                </form>
            </div>
        @endforeach

    </div>
</div>


@endsection