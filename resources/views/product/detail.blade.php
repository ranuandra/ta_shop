@extends('layout.master')

@section('content')

<!-- ==========================
     BREADCRUMB - START
 =========================== -->
<section class="breadcrumb-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <p>{{ $data->nama_product }}</p>
            </div>
            <div class="col-xs-6">
                <ol class="breadcrumb">
                    <li><a href="{{ URL('/') }}">Home</a></li>
                    <li><a href="{{ URL('product/'.$data->kategori_id) }}">{{ $data->GetKategori->nama_kategori }}</a></li>
                    <li class="active">{{ $data->nama_product }}</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- ==========================
    BREADCRUMB - END
=========================== -->

<!-- ==========================
    PRODUCTS - START
=========================== -->
<section class="content products">
    <div class="container">
        <article class="product-item product-single">
            <div class="row">
                <div class="col-xs-4">
                    <div class="product-carousel-wrapper">
                        <div id="product-carousel">
                            <div class="item"><img src="{{ URL::asset('uploads/'.$data->file_image) }}" class="img-responsive" alt=""></div>
                            <div class="item"><img src="{{ URL::asset('uploads/'.$data->file_image) }}" class="img-responsive" alt=""></div>
                            <div class="item"><img src="{{ URL::asset('uploads/'.$data->file_image) }}" class="img-responsive" alt=""></div>
                            <div class="item"><img src="{{ URL::asset('uploads/'.$data->file_image) }}" class="img-responsive" alt=""></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="product-body">
                        <h3>{{ $data->nama_product }}</h3>
                        <div class="product-labels">
                            <span class="label label-info">new</span>
                            <span class="label label-danger">sale</span>
                        </div>
                        <div class="product-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                            <span class="price">
                                <ins><span class="amount">{{ $data->harga }}</span></ins>
                            </span>
                        <ul class="list-unstyled product-info">
                            <li><span>ID</span>U-187423</li>
                            <li><span>Availability</span>In Stock</li>
                            <li><span>Brand</span>Esprit</li>
                            <li><span>Tags</span>Dress, Black, Women</li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat mauris eget magna egestas porta. Curabitur sagittis sagittis neque rutrum congue. Donec lobortis dui sagittis, ultrices nunc ornare, ultricies elit. Curabitur tristique felis pulvinar nibh porta. </p>
                        <div class="product-form clearfix">
                            <div class="row row-no-padding">

                                <div class="col-md-3 col-sm-4">
                                    <div class="product-quantity clearfix">
                                        <a class="btn btn-default" id="qty-minus">-</a>
                                        <input type="text" class="form-control" id="qty" value="1">
                                        <a class="btn btn-default" id="qty-plus">+</a>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <div class="product-size">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <label>Size:</label>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option>XS</option>
                                                    <option>S</option>
                                                    <option selected="selected">M</option>
                                                    <option>L</option>
                                                    <option>XL</option>
                                                    <option>XXL</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <div class="product-color">
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <label>Color:</label>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option selected="selected">Black</option>
                                                    <option>White</option>
                                                    <option>Red</option>
                                                    <option>Yellow</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12">
                                    <a href="#" class="btn btn-primary add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                </div>

                            </div>
                        </div>
                        <ul class="list-inline product-links">
                            <li><a href="#"><i class="fa fa-heart"></i>Add to wishlist</a></li>
                            <li><a href="#"><i class="fa fa-exchange"></i>Compare</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i>Email to friend</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </article>


    </div>
</section>
<!-- ==========================
    PRODUCTS - END
=========================== -->


@endsection