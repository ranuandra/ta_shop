<?php
/**
 * Created by PhpStorm.
 * User: ranuandra
 * Date: 2016/05/27
 * Time: 20:52
 */

namespace App\Helper;

use App\Helper\Contract\MenuProductContract;
use App\KategoriProduct;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;


class MenuProductHelper implements MenuProductContract
{


    public function ProductMenu()
    {
        $products = Product::all();
        return $products;
    }
}