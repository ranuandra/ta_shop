<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 19:48
 */

namespace App\Helper;

use App\Helper\Contract\MenuCartContract;
use App\KategoriProduct;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;


class MenuCartHelper implements MenuCartContract
{


    public function CartMenu()
    {
        $contents = Cart::instance('order');
        return $contents;
    }
}