<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 21:15
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = 'mst_pengiriman';
    protected $fillable = array('nama_pengiriman', 'aktif');

}