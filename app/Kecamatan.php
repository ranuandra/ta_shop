<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:28
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'mst_kecamatan';
    protected $fillable = array('nama_kecamatan', 'kota_id', 'aktif');

    public function GetKota()
    {
        return $this->belongsTo('App\Kota', 'kota_id');
    }

}