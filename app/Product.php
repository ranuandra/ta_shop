<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';
    protected $fillable = array('nama_product', 'kategori_id', 'harga', 'berat', 'file_image', 'aktif');

    public function GetKategori()
    {
        return $this->belongsTo('App\KategoriProduct', 'kategori_id');
    }
}
