<?php

namespace App\Providers;

use App\Helper\MenuKategoriHelper;
use Illuminate\Support\ServiceProvider;

class MenuKategoriServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helper\Contract\MenuKategoriContract', function(){

            return new MenuKategoriHelper();
        });
    }

    public function provides()
    {
        return ['App\Helper\Contract\MenuKategoriContract'];
    }
}
