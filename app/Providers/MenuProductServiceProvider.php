<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 21:01
 */

use Illuminate\Support\ServiceProvider;

class MenuProductServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helper\Contract\MenuProductContract', function(){

            return new \App\Helper\MenuProductHelper();
        });
    }

    public function provides()
    {
        return ['App\Helper\Contract\MenuProductContract'];
    }
}