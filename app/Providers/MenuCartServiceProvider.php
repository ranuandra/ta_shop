<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 19:52
 */

use Illuminate\Support\ServiceProvider;

class MenuCartServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Helper\Contract\MenuCartContract', function(){

            return new \App\Helper\MenuCartHelper();
        });
    }

    public function provides()
    {
        return ['App\Helper\Contract\MenuCartContract'];
    }
}
