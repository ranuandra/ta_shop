<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 21:16
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'mst_pembayaran';
    protected $fillable = array('nama_pembayaran', 'aktif');

}