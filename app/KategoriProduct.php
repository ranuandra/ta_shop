<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduct extends Model
{
    protected $table = 'kategori';
    protected $fillable = array('nama_kategori', 'aktif');

    public function GetProducts()
    {
        return $this->hasMany('App\Product', 'kategori_id');
    }
}
