<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:25
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'mst_provinsi';
    protected $fillable = array('nama_provinsi', 'negara_id', 'aktif');

    public function GetNegara()
    {
        return $this->belongsTo('App\Negara', 'negara_id');
    }

    public function GetKotas()
    {
        return $this->hasMany('App\Kota', 'provinsi_id');
    }
}