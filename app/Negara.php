<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:23
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    protected $table = 'mst_negara';
    protected $fillable = array('nama_negara', 'aktif');

    public function GetProvinsis()
    {
        return $this->hasMany('App\Provinsi', 'negara_id');
    }
}