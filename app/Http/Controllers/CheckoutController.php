<?php
/**
 * Created by PhpStorm.
 * User: ranuandra
 * Date: 2016/05/26
 * Time: 13:35
 */

namespace App\Http\Controllers;

use App\DetailTransaksi;
use App\KategoriProduct;
use App\Kota;
use App\Negara;
use App\Pembayaran;
use App\Pembeli;
use App\Pengiriman;
use App\Product;
use App\Provinsi;
use App\StatusTransaksi;
use App\Transaksi;
use Carbon\Carbon;
use DB;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\CartCollection;
use Gloudemans\Shoppingcart\CartRowCollection;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class CheckoutController extends Controller
{

    public function index(Request $request)
    {

        $id = 1;
        $billing = Cart::instance('billing')->content()->first();
        if(!$billing){
            $billing = null;
        }
        //dd($billing);
        if($request->nama != "" && $request->nama != null){

            if($billing)
            {
                Cart::instance('billing')->update($billing->rowid, array('id' => $id, 'name' => '', 'qty' => 0, 'price' => 0, 'options' => array('nama' => $request->nama, 'alamat' => $request->alamat, 'email' => $request->email, 'no_telp' => $request->no_telp, 'kota_id' => $request->kota_id, 'provinsi_id' => $request->provinsi_id, 'negara_id' => $request->negara_id)));
            } else {
                Cart::instance('billing')->add(array('id' => $id, 'name' => $request->nama, 'qty' => 1, 'price' => 1, 'options' => array('nama' => $request->nama, 'alamat' => $request->alamat, 'email' => $request->email, 'no_telp' => $request->no_telp, 'kota_id' => $request->kota_id, 'provinsi_id' => $request->provinsi_id, 'negara_id' => $request->negara_id)));
            }
            $test = Cart::instance('billing')->content();
            //dd($test);
            return redirect('checkout-pengiriman');
        }


        //$content = Cart::instance('billing')->content();
        //$request->session()->put('backurl', $request->header("referer"));
        $back_url = $request->header("referer");
        if($request->negara_id != "" && $request->negara_id != null){
            $negaraId = $request->negara_id;
        } else {
            $negara = Negara::where('aktif', 1)->first();
            $negaraId = $negara->id;
        }

        $negaras = Negara::where('aktif',1)->lists('nama_negara', 'id');
        $provinsi = Provinsi::where('aktif', 1)->where('negara_id', $negaraId)->first();
        if($request->provinsi_id != "" && $request->provinsi_id != null) {
            $provinsiId = $request->provinsi_id;
        } else {
            $provinsis = Provinsi::where('aktif', 1)->where('negara_id', $negaraId)->lists('nama_provinsi', 'id');
            $provinsiId = $provinsi->id;
        }
        $kotas = Kota::where('aktif', 1)->where('provinsi_id', $provinsiId)->lists('nama_kota','id');

        return view('checkout.billing', compact('billing','back_url', 'negaras', 'provinsis', 'kotas'));
    }

    public function pengiriman(Request $request){
        $id = 1;
        $shipping = Cart::instance('shipping')->content()->first();
        if(!$shipping){
            $shipping = null;
        }
        //dd($billing);
        if($request->pengiriman_id != "" && $request->pengiriman_id != null){
            $pengiriman = Pengiriman::find($request->pengiriman_id);
            if($pengiriman != null) {
                if ($shipping) {
                    Cart::instance('shipping')->associate('Pengiriman', 'App')->update($shipping->rowid, array('id' => $pengiriman->id, 'name' => $pengiriman->nama_pengiriman, 'qty' => 0, 'price' => 0, 'options' => array('pengiriman_id' => $request->pengiriman_id)));
                } else {
                    Cart::instance('shipping')->associate('Pengiriman', 'App')->add(array('id' => $pengiriman->id, 'name' => $pengiriman->nama_pengiriman, 'qty' => 1, 'price' => 1, 'options' => array('pengiriman_id' => $request->pengiriman_id)));
                }
                $test = Cart::instance('shipping')->content();

                return redirect('checkout-pembayaran');
            }
        }



        $pengirimans = Pengiriman::where('aktif', 1)->get();

        return view('checkout.pengiriman', compact('shipping','back_url', 'pengirimans'));
    }

    public function pembayaran(Request $request){
        $id = 1;
        $pembayaran = Cart::instance('pembayaran')->content()->first();
        if(!$pembayaran){
            $pembayaran = null;
        }
        //dd($billing);
        if($request->pembayaran_id != "" && $request->pembayaran_id != null){
            $pembayaranData = Pembayaran::find($request->pembayaran_id);
            if($pembayaranData!=null) {

                if ($pembayaran) {
                    Cart::instance('pembayaran')->associate('Pembayaran', 'App')->update($pembayaran->rowid, array('id' => $pembayaranData->id, 'name' => $pembayaranData->nama_pembayaran, 'qty' => 0, 'price' => 0, 'options' => array('pembayaran_id' => $request->pembayaran_id)));
                } else {
                    Cart::instance('pembayaran')->associate('Pembayaran', 'App')->add(array('id' => $pembayaranData->id, 'name' => $pembayaranData->nama_pembayaran, 'qty' => 1, 'price' => 1, 'options' => array('pembayaran_id' => $request->pembayaran_id)));
                }
                $test = Cart::instance('pembayaran')->content();
                //dd($test);
                return redirect('checkout-konfirmasi');
            }
        }



        $pembayarans = Pembayaran::where('aktif', 1)->get();

        return view('checkout.pembayaran', compact('pembayaran','pembayarans','back_url'));
    }

    public function konfirmasi(Request $request){

        $orders = Cart::instance('order')->content();
        $billing = Cart::instance('billing')->content()->first();
        $shipping = Cart::instance('shipping')->content()->first();
        $pembayaran = Cart::instance('pembayaran')->content()->first();
        if($billing != null){
            $kota = Kota::find($billing->options->kota_id);
        }
        //dd($pembayaran);
        return view('checkout.konfirmasi', compact('orders','billing','shipping', 'pembayaran', 'kota'));
    }

    public function save(Request $request){
        $orders = Cart::instance('order')->content();
        $billing = Cart::instance('billing')->content()->first();
        $shipping = Cart::instance('shipping')->content()->first();
        $pembayaran = Cart::instance('pembayaran')->content()->first();

        DB::transaction(function()use($billing, $shipping, $pembayaran, $orders) {
            $pembeli = new Pembeli();
            $pembeli->nama = $billing->options->nama;
            $pembeli->alamat = $billing->options->alamat;
            $pembeli->email = $billing->options->email;
            $pembeli->kota_id = $billing->options->kota_id;
            $pembeli->no_telp = $billing->options->no_telp;
            $pembeli->save();


            $status = StatusTransaksi::where('aktif', 1)->first();
            $transaksi = new Transaksi();
            $transaksi->pembeli_id = $pembeli->id;
            $transaksi->tanggal_transaksi = Carbon::now();
            $transaksi->total_harga = 0;
            $transaksi->pembayaran_id = $pembayaran->id;
            $transaksi->pengiriman_id = $shipping->id;
            $transaksi->status_transaksi_id = $status->id;
            $transaksi->save();

            foreach ($orders as $order) {
                $fileName = rand(111111111111, 999999999999).'.cdr';
                File::move('tmp_cdr/'.$order->options->filename, 'file_cdr/'.$fileName);
                $detail = new DetailTransaksi();

                $detail->transaksi_id = $transaksi->id;
                $detail->product_id = $order->id;
                $detail->panjang = $order->options->panjang;
                $detail->lebar = $order->options->lebar;
                $detail->berat = $order->product->berat * $order->options->panjang * $order->options->lebar;
                $detail->harga_satuan = $order->product->harga;
                $detail->jumlah = 1;
                $detail->nama_file = $fileName;
                $detail->total_harga = $order->product->berat * $order->options->panjang * $order->options->lebar * $order->product->harga;
                $detail->save();

            }
        });

        return view('checkout.selesai');
    }


}