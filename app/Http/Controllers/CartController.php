<?php
/**
 * Created by PhpStorm.
 * User: ranuandra
 * Date: 2016/05/22
 * Time: 22:45
 */

namespace App\Http\Controllers;

use App\KategoriProduct;
use App\Product;
use DB;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;


class CartController extends Controller
{

    public function index(Request $request)
    {

        //$carts = $request->session()->all();
        if($request->hasFile('file_cdr')) {
            $product = Product::find($request->product_id);
            if ($product) {
                //$extension = $request->file('img_file')->getClientOriginalName()->getClientOriginalExtension();
                $fileName = $request->file('file_cdr')->getClientOriginalName();
                $request->file('file_cdr')->move('tmp_cdr', $fileName);
                Cart::instance('order')->associate('Product', 'App')->add(array('id' => $request->product_id, 'name' => $product->nama_product, 'qty' => 1, 'price' => $product->harga, 'options' => array('panjang' => $request->panjang, 'lebar' => $request->lebar, 'filename' => $fileName)));
            }
        }
        $contents = Cart::instance('order')->content();
        //$request->session()->put('backurl', $request->header("referer"));
        $back_url = $request->header("referer");
        return view('cart.index', compact('contents', 'back_url'));

    }


}