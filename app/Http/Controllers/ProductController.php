<?php
/**
 * Created by PhpStorm.
 * User: ranuandra
 * Date: 2016/05/22
 * Time: 01:31
 */

namespace App\Http\Controllers;

use App\KategoriProduct;
use App\Product;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProductController extends Controller
{

    public function index($id=null)
    {

        if($id == null){
            $kat = KategoriProduct::where('aktif',1)->first();
            $id = $kat->id;
        }
        $data = KategoriProduct::with(['GetProducts' => function($query){
                $query->where('aktif',1)
                      ->paginate(20);
        } ])
            ->where('aktif',1)
            ->where('id', $id)
            ->first();
        return view('product.index', compact('data'));
    }

    public function detail($id){
        $data = Product::with('GetKategori')->where('id', $id)->first();

        return view('product.detail', compact('data'));
    }


}