<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:30
 */

namespace App\Http\Controllers\Admin;



use App\Negara;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class NegaraController extends Controller
{

    public function index()
    {
        $datas = Negara::paginate(20);
        return view('admin.negara.index', compact('datas'));
    }

    public function create()
    {
        $data = new Negara();
        return view('admin.negara.create',compact('data') );
    }

    public function edit($id)
    {
        $data = Negara::find($id);
        return view('admin.negara.create',compact('data') );
    }

    public function save(Request $request)
    {
        if ($request->id != null || $request->id != "") {
            $data = Negara::find($request->id );
            $data->nama_negara = $request->nama_negara;
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }
            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            //dd($data);
            Negara::create($data);
        }
        return redirect('admin/negara');
    }

}