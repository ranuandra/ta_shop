<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:39
 */

namespace App\Http\Controllers\Admin;



use App\Negara;
use App\Provinsi;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KategoriProduct;
use App\Product;


class ProvinsiController extends Controller
{

    public function index()
    {
        $datas = Provinsi::with('GetNegara')->paginate(20);
        return view('admin.provinsi.index', compact('datas'));
    }

    public function create()
    {
        $data = new Provinsi();
        $negaras = Negara::lists('nama_negara', 'id');
        return view('admin.provinsi.create',compact('data', 'negaras') );
    }

    public function edit($id)
    {
        $data = Provinsi::find($id);
        $negaras = Negara::lists('nama_negara', 'id');
        return view('admin.provinsi.create',compact('data', 'negaras') );
    }

    public function save(Request $request)
    {
        if ($request->id != null || $request->id != "") {
            $data = Provinsi::find($request->id );
            $data->nama_provinsi = $request->nama_provinsi;
            $data->negara_id = $request->negara_id;
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }
            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            //dd($data);
            Provinsi::create($data);
        }
        return redirect('admin/provinsi');
    }

}