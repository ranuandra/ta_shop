<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 21:20
 */

namespace App\Http\Controllers\Admin;

use App\KategoriProduct;
use App\Pembayaran;
use App\Pengiriman;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class PembayaranController extends Controller
{

    public function index()
    {
        $datas = Pembayaran::paginate(20);
        return view('admin.pembayaran.index', compact('datas'));
    }

    public function create()
    {
        $data = new Pembayaran();
        return view('admin.pembayaran.create',compact('data') );
    }

    public function edit($id)
    {
        $data = Pembayaran::find($id);
        return view('admin.pembayaran.create',compact('data') );
    }

    public function save(Request $request)
    {

        if ($request->id != null || $request->id != "") {
            $data = Pembayaran::find($request->id );
            $data->nama_pembayaran = $request->nama_pembayaran;
            //dd($request->aktif);
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }

            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            Pembayaran::create($data);
        }
        return redirect('admin/pembayaran');
    }

}