<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 13:57
 */

namespace App\Http\Controllers\Admin;

use App\KategoriProduct;
use App\StatusTransaksi;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class StatusTransaksiController extends Controller
{

    public function index()
    {
        $datas = StatusTransaksi::paginate(20);
        return view('admin.statustransaksi.index', compact('datas'));
    }

    public function create()
    {
        $data = new StatusTransaksi();
        return view('admin.statustransaksi.create',compact('data') );
    }

    public function edit($id)
    {
        $data = StatusTransaksi::find($id);
        return view('admin.statustransaksi.create',compact('data') );
    }

    public function save(Request $request)
    {

        if ($request->id != null || $request->id != "") {
            $data = StatusTransaksi::find($request->id );
            $data->status_transaksi = $request->status_transaksi;
            //dd($request->aktif);
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }

            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            StatusTransaksi::create($data);
        }
        return redirect('admin/status-transaksi');
    }

}