<?php
/**
 * Created by PhpStorm.
 * User: ranuandra
 * Date: 2016/05/21
 * Time: 01:02
 */
namespace App\Http\Controllers\Admin;



use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KategoriProduct;
use App\Product;


class ProductController extends Controller
{

    public function index()
    {
        $datas = Product::with('GetKategori')->paginate(20);
        return view('admin.product.index', compact('datas'));
    }

    public function create()
    {
        $data = new Product();
        $kategories = KategoriProduct::lists('nama_kategori', 'id');
        return view('admin.product.create',compact('data', 'kategories') );
    }

    public function edit($id)
    {
        $data = Product::find($id);
        $kategories = KategoriProduct::lists('nama_kategori', 'id');
        return view('admin.product.create',compact('data', 'kategories') );
    }

    public function save(Request $request)
    {
        if ($request->id != null || $request->id != "") {
            $data = Product::find($request->id );
            $data->nama_product = $request->nama_product;
            $data->kategori_id = $request->kategori_id;
            $data->berat = $request->berat;
            $data->harga = $request->harga;
            $data->deskripsi = $request->deskripsi;
            if($request->hasFile('img_file')){
                $extension = $request->file('img_file')->getClientOriginalName()->getClientOriginalExtension();
                $fileName = rand(111111111111, 999999999999).'.'.$extension;
                $request->file('img_file')->move('uploads', $fileName);
                $data['file_image'] = $fileName;
                //$request->file_image = $fileName;
            }
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }
            //$data->aktif = $request->aktif;
            $data->save();
        } else {


            if($request->hasFile('img_file')){
                $extension = $request->file('img_file')->getClientOriginalExtension();
                $fileName = rand(111111111111, 999999999999).'.'.$extension;
                $request->file('img_file')->move('uploads', $fileName);
                //$request->file_image = $fileName;
            }
            $data = $request->all();
            $data['file_image'] = $fileName;
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            //dd($data);
            Product::create($data);
        }
        return redirect('admin/product');
    }

}