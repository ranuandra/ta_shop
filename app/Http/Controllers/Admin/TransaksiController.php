<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 10:25
 */

namespace App\Http\Controllers\Admin;



use App\StatusTransaksi;
use App\Transaksi;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KategoriProduct;
use App\Product;


class TransaksiController extends Controller
{

    public function index()
    {
        $datas = Transaksi::with('GetPembeli.GetKota.GetProvinsi.GetNegara', 'GetDetails.GetProduct', 'GetPembayaran', 'GetPengiriman', 'GetStatusTransaksi')->paginate(20);
        //dd($datas);
        return view('admin.transaksi.index', compact('datas'));
    }


    public function edit($id)
    {
        $data = Transaksi::with('GetPembeli', 'GetDetails.GetProduct.GetKategori', 'GetPengiriman', 'GetPembayaran', 'GetStatusTransaksi')->find($id);
        $statuses = StatusTransaksi::lists('status_transaksi', 'id');
        return view('admin.transaksi.create',compact('data', 'statuses') );
    }

    public function save(Request $request)
    {
        if ($request->id != null || $request->id != "") {
            $data = Product::find($request->id );
            $data->nama_product = $request->nama_product;
            $data->kategori_id = $request->kategori_id;
            $data->berat = $request->berat;
            $data->harga = $request->harga;
            $data->deskripsi = $request->deskripsi;
            if($request->hasFile('img_file')){
                $extension = $request->file('img_file')->getClientOriginalExtension();
                $fileName = rand(111111111111, 999999999999).'.'.$extension;
                $request->file('img_file')->move('uploads', $fileName);
                $data['file_image'] = $fileName;
                //$request->file_image = $fileName;
            }
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }
            //$data->aktif = $request->aktif;
            $data->save();
        } else {


            if($request->hasFile('img_file')){
                $extension = $request->file('img_file')->getClientOriginalExtension();
                $fileName = rand(111111111111, 999999999999).'.'.$extension;
                $request->file('img_file')->move('uploads', $fileName);
                //$request->file_image = $fileName;
            }
            $data = $request->all();
            $data['file_image'] = $fileName;
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            //dd($data);
            Product::create($data);
        }
        return redirect('admin/transaksi');
    }

}