<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/19
 * Time: 15:22
 */

namespace App\Http\Controllers\Admin;

use App\KategoriProduct;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class KategoriController extends Controller
{

    public function index()
    {
        $datas = KategoriProduct::paginate(20);
        return view('admin.kategori.index', compact('datas'));
    }

    public function create()
    {
        $data = new KategoriProduct();
        return view('admin.kategori.create',compact('data') );
    }

    public function edit($id)
    {
        $data = KategoriProduct::find($id);
        return view('admin.kategori.create',compact('data') );
    }

    public function save(Request $request)
    {

        if ($request->id != null || $request->id != "") {
            $data = KategoriProduct::find($request->id );
            $data->nama_kategori = $request->nama_kategori;
            //dd($request->aktif);
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }

            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            KategoriProduct::create($data);
        }
        return redirect('admin/kategori-product');
    }

}