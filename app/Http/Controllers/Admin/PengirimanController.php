<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 21:16
 */

namespace App\Http\Controllers\Admin;

use App\KategoriProduct;
use App\Pengiriman;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class PengirimanController extends Controller
{

    public function index()
    {
        $datas = Pengiriman::paginate(20);
        return view('admin.pengiriman.index', compact('datas'));
    }

    public function create()
    {
        $data = new Pengiriman();
        return view('admin.pengiriman.create',compact('data') );
    }

    public function edit($id)
    {
        $data = Pengiriman::find($id);
        return view('admin.pengiriman.create',compact('data') );
    }

    public function save(Request $request)
    {

        if ($request->id != null || $request->id != "") {
            $data = Pengiriman::find($request->id );
            $data->nama_pengiriman = $request->nama_pengiriman;
            //dd($request->aktif);
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }

            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            Pengiriman::create($data);
        }
        return redirect('admin/pengiriman');
    }

}