<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:43
 */

namespace App\Http\Controllers\Admin;



use App\Kota;
use App\Negara;
use App\Provinsi;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KategoriProduct;
use App\Product;


class KotaController extends Controller
{

    public function index()
    {
        $datas = Kota::with('GetProvinsi.GetNegara')->paginate(20);
        return view('admin.kota.index', compact('datas'));
    }

    public function create()
    {
        $data = new Kota();
        $negaras = Negara::lists('nama_negara', 'id');
        $negara = Negara::where('aktif', 1)->first();
        $provinsis = Provinsi::where('negara_id',$negara->id)->lists('nama_provinsi', 'id');
        return view('admin.kota.create',compact('data', 'negaras', 'provinsis') );
    }

    public function edit($id)
    {
        $data = Kota::with('GetProvinsi.GetNegara')->find($id);
        $negaras = Negara::where('id', $data->GetProvinsi->GetNegara->id)->lists('nama_negara', 'id');
        $provinsis = Provinsi::where('negara_id', $data->GetProvinsi->negara_id)->lists('nama_provinsi', 'id');
        return view('admin.kota.create',compact('data', 'negaras', 'provinsis') );
    }

    public function save(Request $request)
    {
        if ($request->id != null || $request->id != "") {
            $data = Kota::find($request->id );
            $data->nama_kota = $request->nama_kota;
            $data->provinsi_id = $request->provinsi_id;
            if(!isset($request->aktif)) {
                $data->aktif = 0;
            } else {
                $data->aktif = 1;
            }
            $data->save();
        } else {

            $data = $request->all();
            if(!isset($data['aktif'])){
                $data['aktif'] = 0;
            } else {
                $data['aktif'] = 1;
            }
            //dd($data);
            Kota::create($data);
        }
        return redirect('admin/kota');
    }

}