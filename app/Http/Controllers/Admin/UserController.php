<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/21
 * Time: 22:58
 */



namespace App\Http\Controllers\Admin;



use App\User;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KategoriProduct;
use App\Product;
use Illuminate\Support\Facades\Crypt;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;


class UserController extends Controller
{

    public function index()
    {
        $datas = User::paginate(20);
        return view('admin.user.index', compact('datas'));

    }

    public function create()
    {
        $data = new User();
        $roles = Role::lists('name', 'id');
        $uroles = $data->getRoles();
        $uroles = collect($uroles)->flip()->toArray();
        return view('admin.user.create',compact('data', 'roles', 'uroles') );
    }

    public function edit($id)
    {
        $data = User::find($id);
        $roles = Role::lists('name', 'id');
        $uroles = $data->getRoles();
        $uroles = collect($uroles)->flip()->toArray();
        return view('admin.user.create',compact('data', 'roles', 'uroles') );
    }

    public function save(Request $request)
    {
        $user = new User();
        if($request->id != null)
        {

            $user = User::find($request->id);
        }
        $user->name = $request->name;
        $user->username = $request->username;


        if($request->password != null)
        {
            $user->password = bcrypt($request->password);
        }

        $user->email = $request->email;
        $user->save();

        if($request->id != null) {
            //foreach($request->roles as $role)
            //$user->assignRole($role);
            $user->syncRoles($request->roles);
        } else{
            foreach($request->roles as $role) {
                $user->assignRole($role);
            }

        }

        return redirect('admin/user');
    }

}