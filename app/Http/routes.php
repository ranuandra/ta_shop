<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome');

    });


    //admin/kategori-product
    Route::get('admin/kategori-product',['uses' => 'Admin\KategoriController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::get('admin/kategori-product/create',['uses'=>'Admin\KategoriController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::get('admin/kategori-product/edit/{id}',['uses' => 'Admin\KategoriController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::post('admin/kategori-product/save', ['uses' => 'Admin\KategoriController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);

    //admin/product
    Route::get('admin/product',['uses' => 'Admin\ProductController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::get('admin/product/create',['uses' => 'Admin\ProductController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::get('admin/product/edit/{id}',['uses' => 'Admin\ProductController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);
    Route::post('admin/product/save', ['uses' => 'Admin\ProductController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'
    ]);


    //admin/negara
    Route::get('admin/negara',        ['uses'         => 'Admin\NegaraController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/negara/create',        ['uses'         => 'Admin\NegaraController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/negara/edit/{id}',        ['uses'         => 'Admin\NegaraController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/negara/save',         ['uses'         => 'Admin\NegaraController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);


    //admin/provinsi
    Route::get('admin/provinsi',        ['uses'         => 'Admin\ProvinsiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/provinsi/create',        ['uses'         => 'Admin\ProvinsiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/provinsi/edit/{id}',        ['uses'         => 'Admin\ProvinsiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/provinsi/save',         ['uses'         => 'Admin\ProvinsiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);

    //admin/kota
    Route::get('admin/kota',        ['uses'         => 'Admin\KotaController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/kota/create',        ['uses'         => 'Admin\KotaController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/kota/edit/{id}',        ['uses'         => 'Admin\KotaController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/kota/save',         ['uses'         => 'Admin\KotaController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);


    //admin/pengiriman
    Route::get('admin/pengiriman',        ['uses'         => 'Admin\PengirimanController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/pengiriman/create',        ['uses'         => 'Admin\PengirimanController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/pengiriman/edit/{id}',        ['uses'         => 'Admin\PengirimanController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/pengiriman/save',         ['uses'         => 'Admin\PengirimanController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);


    //admin/pembayaran
    Route::get('admin/pembayaran',        ['uses'         => 'Admin\PembayaranController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/pembayaran/create',        ['uses'         => 'Admin\PembayaranController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/pembayaran/edit/{id}',        ['uses'         => 'Admin\PembayaranController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/pembayaran/save',         ['uses'         => 'Admin\PembayaranController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);


    //admin/status-transaksi
    Route::get('admin/status-transaksi',        ['uses'         => 'Admin\StatusTransaksiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/status-transaksi/edit/{id}',        ['uses'         => 'Admin\StatusTransaksiController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/status-transaksi/create',        ['uses'         => 'Admin\StatusTransaksiController@create',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::post('admin/status-transaksi/save',        ['uses'         => 'Admin\StatusTransaksiController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);

    //admin/status-transaksi
    Route::get('admin/transaksi',        ['uses'         => 'Admin\TransaksiController@index',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);
    Route::get('admin/transaksi/edit/{id}',        ['uses'         => 'Admin\TransaksiController@edit',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);

    Route::post('admin/transaksi/save',        ['uses'         => 'Admin\TransaksiController@save',
        'middleware'   => ['auth', 'acl'],
        'is'           => 'administrator'

    ]);


    //admin/user
    Route::get('admin/user',
        ['uses'         => 'Admin\UserController@index',
         'middleware'   => ['auth', 'acl'],
         'is'           => 'administrator'

        ]);
    Route::get('admin/user/create',
        ['uses'         => 'Admin\UserController@create',
            'middleware'   => ['auth', 'acl'],
            'is'           => 'administrator'

        ]);
    Route::get('admin/user/edit/{id}',
        ['uses'         => 'Admin\UserController@edit',
            'middleware'   => ['auth', 'acl'],
            'is'           => 'administrator'

        ]);
    Route::post('admin/user/save',
        ['uses'         => 'Admin\UserController@save',
            'middleware'   => ['auth', 'acl'],
            'is'           => 'administrator'

        ]);

    Route::match(['get','post'],'provinsi/data/harga-komoditas/{id?}',
        [   'uses'           => 'Provinsi\HargaKomoditasController@index',
            'middleware'  => ['auth', 'acl'],
            'is'          => 'provinsi'
        ])->where('id', '[0-9]+');;


    Route::get('provinsi/data/harga-komoditas',
        ['uses'         => 'Provinsi\HargaKomoditasController@index',
            'middleware'   => ['auth', 'acl'],
            'is'           => 'provinsi'
        ]);

    Route::get('product/{id?}', 'ProductController@index');
    Route::get('product-detail/{id?}', 'ProductController@detail');
    Route::match(['get','post'], 'cart', 'CartController@index');
    Route::match(['get','post'], 'checkout', 'CheckoutController@index');
    Route::match(['get','post'], 'checkout-pengiriman', 'CheckoutController@pengiriman');
    Route::match(['get','post'], 'checkout-pembayaran', 'CheckoutController@pembayaran');
    Route::match(['get','post'], 'checkout-konfirmasi', 'CheckoutController@konfirmasi');
    Route::match(['get','post'], 'checkout-save', 'CheckoutController@save');

    //Auth route
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');


// Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');
    Route::get('logout', function(){
        Auth::logout();
        return view('welcome');
    });
});

