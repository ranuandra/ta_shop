<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 20:27
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'mst_kota';
    protected $fillable = array('nama_kota', 'provinsi_id', 'aktif');

    public function GetProvinsi()
    {
        return $this->belongsTo('App\Provinsi', 'provinsi_id');
    }

    public function GetKecamatans()
    {
        return $this->hasMany('App\Kecamatan', 'kota_id');
    }
}