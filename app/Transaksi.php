<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 22:23
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    //
    protected $table = 'transaksi';
    protected $fillable = array('pembeli_id', 'total_harga', 'tanggal_transaksi', 'pembayaran_id', 'pengiriman_id', 'status_transaksi_id');

    public function GetPembeli()
    {
        return $this->belongsTo('App\Pembeli', 'pembeli_id');
    }

    public function GetPembayaran(){
        return $this->belongsTo('App\Pembayaran', 'pembayaran_id');
    }

    public function GetPengiriman(){
        return $this->belongsTo('App\Pengiriman', 'pengiriman_id');
    }

    public function GetDetails(){
        return $this->hasMany('App\DetailTransaksi', 'transaksi_id');
    }

    public function GetStatusTransaksi()
    {
        return $this->belongsTo('App\StatusTransaksi', 'status_transaksi_id');
    }


}