<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/27
 * Time: 13:55
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusTransaksi extends Model
{
    protected $table = 'status_transaksi';
    protected $fillable = array('status_transaksi', 'aktif');

    public function GetNegara()
    {
        return $this->belongsTo('App\Negara', 'negara_id');
    }

    public function GetTransaksis()
    {
        return $this->hasMany('App\Transaksi', 'status_transaksi_id');
    }
}