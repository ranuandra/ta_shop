<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 22:28
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    //
    protected $table = 'detail_transaksi';
    protected $fillable = array('transaksi_id', 'product_id', 'panjang', 'lebar', 'berat', 'harga_satuan', 'jumlah',  'total_harga', 'nama_file');

    public function GetTransaksi()
    {
        return $this->belongsTo('App\Transaksi', 'transaksi_id');
    }

    public function GetProduct(){
        return $this->belongsTo('App\Product', 'product_id');
    }


}