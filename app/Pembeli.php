<?php
/**
 * Created by PhpStorm.
 * Date: 2016/05/26
 * Time: 22:20
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    //
    protected $table = 'pembeli';
    protected $fillable = array('nama', 'alamat', 'email', 'no_telp', 'kota_id');

    public function GetTransaksis(){
        return $this->hasMany('App\Transaksi', 'pembeli_id');
    }

    public function GetKota()
    {
        return $this->belongsTo('App\Kota', 'kota_id');
    }
}